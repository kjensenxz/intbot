# qemu net setup config

# qemu devices
# XXX; to be randomized and assigned per VM

TAP='tapQEMU0'
BR='brQEMU0'

#
# modify these lines
#

# physical LAN device
PHY='eth0'
# HOST ip & subnet (guest is assigned by guest)
ADDR='10.69.0.8/16'
# default gateway
ROUT='10.69.0.1'
