; init.s - simple init for amd64 linux platforms
; simply mounts some filesystems and runs /bin/sh
; Copyright (C) 2017 Kenneth B. Jensen <kj@0x5f3759df.xyz>

; BUILDING:
; $ nasm -f elf64 init.s
; $ ld init.o -o init

BITS 64
CPU x64

section .text
global _start

%macro mount 3
	; make mountpoint & mount fs

	mov rax, 0x53   ; sys_mkdir
	mov rdi, %1     ; location
	mov rsi, %3     ; mode
	syscall

	mov rax, 0xA5   ; sys_mount
;	mov rdi, 0      ; device name
	mov rsi, %1     ; location
	mov rdx, %2     ; filesystem type
;	mov r10, 0      ; flags
;	mov r8,  0      ; data
	syscall
%endmacro

_start:
	; mount filesystems
	mount p_path, p_fs, 755o ; /proc
	mount s_path, s_fs, 755o ; /sys
	mount t_path, t_fs, 777o ; /tmp

	; exec /bin/sh
	; see man 2 execve
	mov rax, 0x3B    ; sys_execve
	mov rdi, sh_path ; filename
	mov rsi, sh_argv ; argv
	xor rdx, rdx     ; envp
	syscall

	; end of program, /bin/sh exited
	mov rax, 0x3C    ; sys_exit
	mov rdi, 1       ; status (EXIT_FAILURE)
	syscall


p_path:
	db "/proc", 0
p_fs:
	db "proc", 0

s_path:
	db "/sys", 0
s_fs:
	db "sysfs", 0

t_path:
	db "/tmp", 0
t_fs:
	db "ramfs", 0


sh_path:
	db "/bin/sh", 0
sh_argv:
	dq sh_path, 0
