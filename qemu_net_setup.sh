#!/usr/bin/env bash
source ./config.sh

function usage() {
	>&2 echo -e "Usage: $1 {init[ialize]|de[init[ialize]]}\n"         \
	            "initialize creates necessary tap & bridge devices\n" \
	            "deinitialize destroys them\n"                        \
	            "note that this script requires escalation"
	exit 1;
}

function init() {
	# $UID is already defined
	GID=$(id -g);

	# just in case
	sudo modprobe tun tap;

	# create tap device for qemu
	sudo ip tuntap add dev ${TAP} mode tap \
		user ${UID}  \
		group ${GID};

#	sudo ip link set dev ${TAP} up #move to qemu run script

	# create bridge device for switching
	sudo ip link add ${BR} type bridge;

	# bridge tap and physical LAN device
	sudo ip link set dev ${TAP} master ${BR};
	sudo ip link set dev ${PHY} master ${BR};

	# move physical device address to bridge
	sudo ip address add ${ADDR} dev ${BR};

	# remove routes & addresses to PHY before assigning
	# route to bridge to prevent errors
	sudo ip route delete default via ${ROUT} dev ${PHY};
	sudo ip address delete ${ADDR} dev ${PHY}
	# ensure that bridge device is up; else the route cannot be added
	sudo ip link set dev ${BR} up;
	sudo ip route add default via ${ROUT} dev ${BR}

}

function deinit() {
	# remove tap and physical LAN device bridging
	sudo ip link set dev ${TAP} nomaster;
	sudo ip link set dev ${PHY} nomaster;

	# deconstruct bridge & tap device
	sudo ip address flush dev ${BR};
	sudo ip link delete dev ${BR};
	sudo ip tuntap del dev ${TAP} mode tap;

	# recreate physical device IP address
	sudo ip address add ${ADDR} dev ${PHY};
	sudo ip route add default via ${ROUT};
}

shopt -s extglob

case $1 in
	init?(ialize))
		init
		;;
	de?(init?(ialize)))
		deinit
		;;
	*)
		usage $0
		;;
esac


