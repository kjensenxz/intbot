#!/bin/bash
source ./config.sh

./qemu_net_setup.sh init

qemu-system-x86_64                                                     \
	-m 1G                                                          \
	-net nic,model=virtio                                          \
	-net tap,ifname=${TAP},script=./qemu_net_init.sh,downscript=no \
	-serial mon:stdio                                              \
	-nographic                                                     \
	-kernel ./builds/bzImage                                       \
	-initrd ./builds/initramfs.cpio.gz                             \
	-append "init=/bin/init console=ttyS0,9600n8 loglevel=0"

./qemu_net_setup.sh deinit
