
.PHONY: all builds run clean

rootdir := $(shell pwd)

builds:
	mkdir -p builds/root/bin/

init:
	nasm -f elf64 src/init.s -o builds/init.o
	ld builds/init.o -o builds/root/bin/init

builds/busybox:
	cp configs/busybox.config contrib/busybox/.config
	cd contrib/busybox/; make --jobs 8 --load-average 7
	cp contrib/busybox/busybox $(rootdir)/builds/busybox

builds/bzImage:
	cp configs/linux.config contrib/linux/.config
	cd contrib/linux/; make --jobs 8 --load-average 7
	cp contrib/linux/arch/x86/boot/bzImage $(rootdir)/builds/bzImage

builds/root: builds builds/busybox builds/bzImage init
	cd $(rootdir)/builds/; ./busybox --install root/bin/

initramfs.cpio.gz: builds/root 
	(cd builds/root; find . | cpio -o -H newc; ) | gzip -9 > builds/$(@)

run:
	./run_qemu.sh

all: initramfs.cpio.gz
	git submodule init

clean:
	rm -rf builds
	cd $(rootdir)/contrib/linux/; make mrproper
	cd $(rootdir)/contrib/busybox/; make mrproper

mrproper:
	rm -rf contrib/
